var sess;
var actualSess;

//User: codysurgroup1
//Pass: 123456

function getNotificaciones() {
    actualSess = wialon.core.Session.getInstance();
    actualSess.initSession("https://hst-api.wialon.com");
    //var token = "242c6d0a408aeefb009d4e76f33922534FB00916568273DC906FD794E3FE7D5AE0367C24";
    var token = "242c6d0a408aeefb009d4e76f33922539517FE6B03D69E4BC34FE8C3CE9FE787F7FDE6D1";
    var account = "monitoreo CCT1"; 
    actualSess.loginToken(token, account,
        function (code) {
            if (code) {

                console.log("error en main "+wialon.core.Errors.getErrorText(code));
                return;
            }
            console.log("Logged successfully");
            obtenerGrupos();
            traerNotificaciones();
        });
}

function traerNotificaciones() {
    sess = wialon.core.Session.getInstance();
    var flags = wialon.item.Item.dataFlag.base | wialon.item.Resource.dataFlag.base | wialon.item.Item.dataFlag.messages | wialon.item.Resource.dataFlag.notifications;

    sess.loadLibrary("resourceNotifications");

    sess.updateDataFlags( // load items to current session
        [{
            type: "type",
            data: "avl_resource",
            flags: flags,
            mode: 1
        },
        {
            type: "type",
            data: "avl_unit",
            flags: flags,
            mode: 0
        },
        {
            type: "type",
            data: "avl_unit_group",
            flags: flags,
            mode: 0
        }
        ],
        function (code) {
            if (code) {
                console.log(wialon.core.Errors.getErrorText(code));
                return;
            }

            var res = sess.getItems("avl_resource");

            for (var i = 0; i < res.length; i++) { // construct Select list using found resources
                res[i].addListener("messageRegistered", showData); // register event when we will receive message
            }
        });
}

function showData(event) {

    var grupos = "";
    var modulos = ""; 
    var data = event.getData();
    console.log(data);
    if (data.tp && data.tp == "unm") {
        var auxCaracter = data.name;

        var auxval = auxCaracter.indexOf("~", auxCaracter.length - 1);

        if (auxval != -1) {
            setTimeout(pintarTabla,60000);

            function pintarTabla(){
                if (typeof cantNot !== 'undefined' && jQuery.isFunction(cantNot)) {
                    //totalnotf();
                    cantNot();
                    if (typeof actualizar !== 'undefined' && jQuery.isFunction(actualizar)) {
                        actualizar();
                    }
                }
            }
        }
    }
}

function obtenerGrupos() {
    sess = wialon.core.Session.getInstance();
    contUnidades(sess);
}

function contUnidades(estadAccess) {

    var unitFlags = wialon.item.Item.dataFlag.base | wialon.item.Unit.dataFlag.lastMessage | wialon.item.Item.image;
    var groupFlags = wialon.item.Item.dataFlag.base;

    estadAccess.loadLibrary("itemIcon");
    estadAccess.updateDataFlags(
        [{
            type: "type",
            data: "avl_unit",
            flags: unitFlags,
            mode: 0
        },
        {
            type: "type",
            data: "avl_unit_group",
            flags: groupFlags,
            mode: 0
        }
        ],
        function (code) {
            if (code) {
                console.log(wialon.core.Errors.getErrorText(code));
                return;
            }

            var groups = estadAccess.getItems("avl_unit_group");
            var units = estadAccess.getItems("avl_unit");

            if (!groups || !groups.length) {
                console.log("Unit groups not found");
                return;
            }

            for (var i = 0; i < groups.length; i++) {
                var gr = groups[i];

                if (gr.getName() === "[CCT] Bloqueo de motor") {
                    var unitsgroupsBM = gr.getUnits();
                    searchUnitBM(unitsgroupsBM, units);
                    var BloqueoMotor = gr.getId();
                } else if (gr.getName() === "[CCT] Unidades sin conexión") {
                    var unitsgroupsUSC = gr.getUnits();
                    searchUnitUSC(unitsgroupsUSC, units);
                    var unidadesSC = gr.getId();
                } else if (gr.getName() === "[CCT] Zona de baja cobertura") {
                    var unitsgroupsBC = gr.getUnits();
                    searchUnitBC(unitsgroupsBC, units);
                    var uniBajaCob = gr.getId();
                } else if (gr.getName() === "[CCT] Unidades detenidas") {
                    var unitsgroupsUD = gr.getUnits(); 
                    searchUnitUD(unitsgroupsUD, units);
                    var uniDet = gr.getId();
                }
            }

            fillGroup(BloqueoMotor, unidadesSC, uniBajaCob, uniDet);
        }
    );
}

function searchUnitBM(unitsgroups, units) {
    var div = document.getElementById("list_BM");
    if (div !== null) {
        div.innerHTML = "";
        for (var i = 0; i < units.length; i++) {
            var u = units[i];
            unitsgroups.forEach(unitgroup => {
                if (u.getId() === unitgroup) {
                    $("#list_BM").append(`<li class="list-group-item py-0">${u.getName()}</li>`);
                }
            });
        }
    }
};

function searchUnitUSC(unitsgroups, units) {
    var div = document.getElementById("list_USC");
    if (div !== null) {
        div.innerHTML = "";
        for (var i = 0; i < units.length; i++) {
            var u = units[i];
            unitsgroups.forEach(unitgroup => {
                if (u.getId() === unitgroup) {
                    $("#list_USC").append(`<li class="list-group-item py-0">${u.getName()}</li>`);
                }
            });
        }
    }
};

function searchUnitBC(unitsgroups, units) {
    var div = document.getElementById("list_UBC");
    if (div !== null) {
        div.innerHTML = "";
        for (var i = 0; i < units.length; i++) {
            var u = units[i];
            unitsgroups.forEach(unitgroup => {
                if (u.getId() === unitgroup) {
                    $("#list_UBC").append(`<li class="list-group-item  py-0">${u.getName()}</li>`);
                }
            });
        }
    }
};

function searchUnitUD(unitsgroups, units) {
    var div = document.getElementById("list_UD");
    if (div !== null) {
        div.innerHTML = "";
        for (var i = 0; i < units.length; i++) {
            var u = units[i];
            unitsgroups.forEach(unitgroup => {
                if (u.getId() === unitgroup) {
                    $("#list_UD").append(`<li class="list-group-item py-0">${u.getName()}</li>`);
                }
            });
        }
    }
};

function fillGroup(BloqueoMotor, unidadesSC, uniBajaCob, uniDet) {
    bloqMotor(BloqueoMotor);
    sinConexion(unidadesSC);
    bajaCobertura(uniBajaCob);
    uniDetenidas(uniDet);
}

function bloqMotor(val) {

    if (val) {
        var group = wialon.core.Session.getInstance().getItem(val);
        if (!group) {
            console.log("Unknown group id: " + val);
            return;
        }
        var units = group.getUnits();

        if (typeof spanBM !== 'undefined') {
            if (units.length === 0) {
                spanBM.innerHTML = 0;
            } else {
                for (var i = 0; i < units.length; i++) {
                    var unit = wialon.core.Session.getInstance().getItem(units[i]);
                    if (!unit)
                        continue;
                    spanBM.innerHTML = i + 1;
                }
            }
        }
    }
}

function sinConexion(val) {

    if (val) {
        var group = wialon.core.Session.getInstance().getItem(val);
        if (!group) {
            console.log("Unknown group id: " + val);
            return;
        }
        var units = group.getUnits();

        if (typeof spanUSC !== 'undefined') {
            if (units.length === 0) {
                spanUSC.innerHTML = 0;
            } else {
                for (var i = 0; i < units.length; i++) {
                    var unit = wialon.core.Session.getInstance().getItem(units[i]);
                    if (!unit)
                        continue;
                    spanUSC.innerHTML = i + 1;
                }
            }
        }
    }
}

function bajaCobertura(val) {

    if (val) {
        var group = wialon.core.Session.getInstance().getItem(val);
        if (!group) {
            console.log("Unknown group id: " + val);
            return;
        }
        var units = group.getUnits();

        if (typeof spanZBC !== 'undefined') {
            if (units.length === 0) {
                spanZBC.innerHTML = 0;
            } else {
                for (var i = 0; i < units.length; i++) {
                    var unit = wialon.core.Session.getInstance().getItem(units[i]);
                    if (!unit)
                        continue;
                    spanZBC.innerHTML = i + 1;
                }
            }
        }
    }
}

function uniDetenidas(val) {

    if (val) {
        var group = wialon.core.Session.getInstance().getItem(val);
        if (!group) {
            console.log("Unknown group id: " + val);
            return;
        }
        var units = group.getUnits();

        if (typeof spanUD !== 'undefined') {
            if (units.length === 0) {
                spanUD.innerHTML = 0;
            } else {
                for (var i = 0; i < units.length; i++) {
                    var unit = wialon.core.Session.getInstance().getItem(units[i]);
                    if (!unit)
                        continue;
                    spanUD.innerHTML = i + 1;
                }
            }
        }
    }
}

setInterval(obtenerGrupos, 50000);

$(document).ready(function () {
    getNotificaciones();
});