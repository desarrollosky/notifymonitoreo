const connection = require('../config/db');
var express = require('express');
var app = express();
const { isLoggedIn } = require('../config/auth');
const router = express.Router();
var moment = require('moment');

const index = "/notifyMonitoreo/";

module.exports = function (express, globals, port) {

    routes = {
        index: function (req, res, next) {
            content = {
                title: 'Notificaciones',
                globals: encodeURIComponent(JSON.stringify(globals)), //Utilizado para transferir las variables globales a la vista y poder utilizarlas en
            };
        },
        notificaciones: function (req, res, next) {
            let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=1 OR estado=2 ORDER BY estado ASC, id DESC;";
            sql += "SELECT * FROM actividad where nombre != 'Otra' order by nombre asc;";
            sql += "SELECT * FROM estado LIMIT 3;";
            sql += "SELECT * FROM comunicacion;";
            sql += "SELECT * FROM accion;";

            let query = connection.query(sql, (err, results) => {

                if (err) {
                    throw err;
                } else {
                    res.render('notificacion/index', {
                        results: results[0],
                        actividades: results[1],
                        estados: results[2],
                        comunicacion: results[3],
                        accion: results[4],
                        usuario: req.user,
                        activoUrg: 1
                    });
                }

            });

        }
    };

    router.get('/index', isLoggedIn, (routes.index, routes.notificaciones));
    return router;
}

//SUBE LOS COMENTARIOS
module.exports.post_agregarComent = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot = req.body.idNot;
    let actividad = req.body.actividad;
    let estado = req.body.estado;
    let aviso = req.body.aviso;
    let comunicacion = req.body.comunicacion;
    let comentarios = req.body.comentarios;
    let user = req.user.id;
    console.log('Se imprime el valor del req.body: ', req.body);
    console.log('Este es el comentario: ', req.body.comentarios);
    console.log('\nEste es el tamaño: ', req.body.comentarios.length);

    var datetime = new Date();
    var day = datetime.getDate();
    var month = datetime.getMonth() + 1;
    var year = datetime.getFullYear();
    var hour = datetime.getHours();
    var minutes = datetime.getMinutes();
    var seconds = datetime.getSeconds();

    var fecha = year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds;

    if (user === "" || user === false && (comentarios === '' || comentarios === null || comentarios === undefined || comentarios.length === 0)) {
        console.log('No traía usuario o el comentario iba vacío: ', req.body);
        res.json({msg: 'error', msg_txt: 'Se encontró un error en el proceso, puede que el comentario se haya ido vacío, \ncomentario: ' + comentarios});
    } else {
        if (aviso === "si") {
            connection.query("INSERT INTO comentario (idNotificacion, idUsuario, idComunicacion, idActividad, texto, fecha) VALUES (?, ?, ?, ?, ?, ?); UPDATE notificacion SET estado =?, texto=? WHERE id =?", [idNot, user, comunicacion, actividad, comentarios, fecha, estado, comentarios, idNot], function (err, result) {
                if (err) {
                    res.json({msg: 'error', msg_txt:'Se encontró un error en el proceso'});
                } else {
                    res.json({msg: 'success', msg_txt:'El comentario se agregó correctamente'});
                }
            });
        } else {
            connection.query("INSERT INTO comentario (idNotificacion, idUsuario, idComunicacion, idActividad, texto, fecha) VALUES (?, ?, ?, ?, ?, ?); UPDATE notificacion SET estado =?, texto=? WHERE id =?", [idNot, user, '5', actividad, comentarios, fecha, estado, comentarios, idNot], function (err, result) {
                if (err) {
                    res.json({msg: 'error', msg_txt:'Se encontró un error en el proceso'});
                } else {
                    res.json({msg: 'success', msg_txt:'El comentario se agregó correctamente'});
                }
            });
        }
    }


}

//MUESTRA EL HISTORIAL
module.exports.post_historialComent = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {
        "coment": "",
        "msj": ""
    };

    connection.query("SELECT comentario.fecha, comunicacion.nombre as comunicacion, actividad.nombre as nombreAct, comentario.texto, usuario.nombre, usuario.apellido FROM comentario, usuario, comunicacion, actividad WHERE comentario.idUsuario=usuario.id AND comunicacion.id=comentario.idComunicacion AND comentario.idActividad=actividad.id AND comentario.idNotificacion=? ORDER BY fecha DESC;", [idNot2], function (err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["coment"] = rows;
                res.json(data);
            } else {
                data["msj"] = 'No existen comentarios en la notificación';
                res.json(data);
            }
        }
    });
}

//INSERTA NOTIFICACIONES
module.exports.insertar_notificaciones = async function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    let name = req.body.name;
    let lon = req.body.lon;
    let lat = req.body.lat;
    let unidad = req.body.unidad;
    let groups = req.body.groups;
    let mod = req.body.mod;
    let timeNot = req.body.timeNot;
    var data;

    let fecha = moment.unix(timeNot).format('YYYY-MM-DD HH:mm:ss');

    // for (var i=0; i < groups.length; i++) {
    //     document.write(groups[i] + ",");
    // }

    await connection.query("SELECT * FROM notificacion WHERE fecha=? AND idUnidad=? AND nombre=?;", [fecha, unidad, name], async function (err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length === 0) {
                await connection.query("INSERT INTO notificacion(idUnidad, estado, fecha, nombre, texto, latitude, longitude, grupo) VALUES(?, ?, ?, ?, ?, ?, ?, ?);", [unidad, '1', fecha, name, mod, lat, lon, groups], function (err, rows, fields) {
                    if (err) { throw err; } else {
                        data = 'OK';
                        res.json(data);
                    }
                });
            }
        }
    });
}

module.exports.traer_notificaciones = function (req, res) {
    res.setHeader('Content-type', 'text/plain');

    //Original
    let sql = "SELECT id, idUnidad, categoria as cat, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=1  and (substring(notificacion.nombre,1,3)) != '[M]' ORDER BY estado ASC, id DESC;";
    //Optimizado
    // let sql = `select n.id, n.idUnidad, n.nombre as nombreNotif, n.texto, n.latitude, n.longitude, n.grupo, n.fecha, 
    // n.nombre as nombreNotif, e.nombre as nombreEst, e.clase, 
    // timediff(c.fecha, n.fecha) as apc, 
    // timediff(max(c.fecha), min(c.fecha)) as auc,
    // CONCAT(u.nombre,' ',u.apellido) as usuario
    // from notificacion n 
    // inner join estado e on e.id = n.estado 
    // left join comentario c on c.idNotificacion = n.id
    // inner join usuario u on u.id = c.idUsuario 
    // WHERE n.estado=1  
    // and (substring(n.nombre,1,3)) != '[M]'
    // group by n.id
    // ORDER BY n.estado ASC, n.id DESC;`;

    connection.query(sql, function (err, rows, fields) {
        if (err) { res.json([]); throw err; } else {
            if (rows.length > 0) {
                for (const i in rows) {
                    let element = rows[i];

                    if (element.grupo != null && element.grupo != ''){
                        let grupo = element.grupo.split(",");
                        if (grupo.length >= 2){
                            element.grupo = grupo[0]+', '+grupo[1];
                        } else{
                            element.grupo = grupo[0];
                        }
                    } else{
                        element.grupo = '';
                    }
                    let color = '';
                    if(element.cat == 'Máxima peligrosidad'){
                        color = 'maxima'; 
                    }else if(element.cat == 'Alta peligrosidad'){
                        color = 'alta';  
                    }else if(element.cat == 'Baja Peligrosidad'){
                        color = 'baja'; 
                    }else{
                        color = ''; 
                    }
                    element.ubicacion = `<a href="https://maps.google.com/?q=${element.longitude},${element.latitude}" target="_blank">${element.nombreNotif}</a>`;
                    element.comentario = `<button type="button" class="btn btn-primary ${color}" data-toggle="tooltip" data-placement="bottom" title="Comentarios" onclick="idPass(${element.id})"><i class="fa fa-edit"></i></button>`;
                }
                res.json(rows);
            } else {
                res.json([]);
            }
        }
    });
}


module.exports.enviarNoti_papelera = function (req, res) {
    res.setHeader('Content-type', 'text/plain');

    connection.query("UPDATE notificacion SET estado=4 where estado=1;", function (err, result) {
        if (err) {
            console.log(err);
            /*data = 'Error';
            res.json(data);*/
            req.flash('error', 'Se encontró un error en el proceso');
            res.redirect(index + 'notificaciones/index');
        } else {
            /*data = 'OK';
            res.json(data);*/
            req.flash('success', 'Las notificaciones fueron enviados a papelera correctamente');
            res.redirect(index + 'notificaciones/index');
        }
    });

}

//MUESTRA EL HISTORIAL
module.exports.post_accion = function (req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {
        "coment": "",
        "msj": ""
    };

    connection.query("select CASE WHEN n.nombre LIKE '%SOS%' THEN (select GROUP_CONCAT(accion SEPARATOR ',\n') from nuevogestor_monitoreo.accion a where a.id_accion in (1,4)) WHEN n.nombre LIKE '%AR3 Parada prohibida zona Zar%' THEN (select GROUP_CONCAT(accion SEPARATOR ',\n') from nuevogestor_monitoreo.accion a where a.id_accion in (2,5,1,3,4)) WHEN n.nombre LIKE '%AR1.2%' THEN (select GROUP_CONCAT(accion SEPARATOR ',\n') from nuevogestor_monitoreo.accion a where a.id_accion in (1,2)) WHEN n.nombre LIKE '%DESVÍO DE RUTA%' THEN (select GROUP_CONCAT(accion SEPARATOR ',\n') from nuevogestor_monitoreo.accion a where a.id_accion in (4,10,2,3,5,1,13)) WHEN n.nombre LIKE '%AR5 Parada prohibida%' THEN (select GROUP_CONCAT(accion SEPARATOR ',\n') from nuevogestor_monitoreo.accion a where a.id_accion in (3,2,9,1,4)) when n.nombre LIKE '%%' then '' END as comentario from nuevogestor_monitoreo.notificacion n where n.id =?;", [idNot2], function (err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["coment"] = rows;
                res.json(data);
            } else {
                data["msj"] = 'No existen acciones para esta notificación.';
                res.json(data);
            }
        }
    });
}