const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');

const index = "/notifyMonitoreo/";

module.exports = function(express, globals) {
    const router = express.Router();

    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Administrar monitoristas',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('administrar_monitoristas/index', content);
        },
        monitoristas: function(req, res, next) {
            let sql = "SELECT id, idRol, nombre, apellido, username, estado FROM usuario WHERE idRol='3';";

            let query = connection.query(sql, (err, results) => {
                if (err) {
                    throw err;
                } else {
                    if (results.length != 0) {
                        res.render('administrar_monitoristas/index', {
                            monitoristas: results,
                            usuario: req.user,
                            activoAdmm: 1
                        });
                    } else {
                        res.render('administrar_monitoristas/index', {
                            monitoristas: 'vacio',
                            usuario: req.user,
                            activoAdmm: 1
                        });
                    }

                }
            });
        }
    };

    router.get('/index', isLoggedIn, (routes.index, routes.monitoristas));
    return router;
};

module.exports.actdes_monitorista = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let id = req.body.id;
    let checked = req.body.checked;
    let userRol = req.user.idRol;

    var data = {
        "class": "",
        "msj": ""
    };

    if (userRol === 1 || userRol === 2) {
        if (checked === 'true') {
            connection.query("UPDATE usuario SET estado = '1' WHERE id = ?;", [id], function(err, result) {
                if (err) {
                    data["class"] = 'alert-danger';
                    data["msj"] = 'Ocurrió un error en el sistema.';
                    res.json(data);
                } else {
                    data["class"] = 'alert-success';
                    data["msj"] = 'Se modificó el acceso del usuario correctamente.';
                    res.json(data);
                }
            });
        } else {
            connection.query("UPDATE usuario SET estado = '0' WHERE id = ?;", [id], function(err, result) {
                if (err) {
                    data["class"] = 'alert-danger';
                    data["msj"] = 'Ocurrió un error en el sistema.';
                    res.json(data);
                } else {
                    data["class"] = 'alert-success';
                    data["msj"] = 'Se modificó el acceso del usuario correctamente.';
                    res.json(data);
                }
            });
        }
    } else {
        data["class"] = 'alert-danger';
        data["msj"] = 'Se necesitan permisos para realizar modificaciones.';
        res.json(data);
    }
}