const { isLoggedIn } = require('../config/auth');

module.exports = function (express,globals) {
    const router = express.Router();
    routes = {
        index: function (req, res, next) {
            content = {
                title: 'Skydash',
                globals:encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('skydash/index', content);
        },
        edit: function (req, res, next) {
            content = {
                title: 'Skydash',
                globals:encodeURIComponent(JSON.stringify(globals)),
                usuario: req.user,
            };
            res.render('skydash/edit', content);
        }
    };
    router.get('/index', isLoggedIn, routes.index);
    router.get('/edit', isLoggedIn, routes.edit);

    return router;
};