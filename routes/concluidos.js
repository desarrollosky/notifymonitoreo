const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');

const index = "/notifyMonitoreo/";

module.exports = function(express, globals) {
    const router = express.Router();
    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Concluidos',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('concluidos/index', content);
        },
        notificaciones: function(req, res, next) {
            let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=3 ORDER BY estado ASC, id DESC;";
            sql += "SELECT * FROM estado WHERE id='1' OR id='2';";

            let query = connection.query(sql, (err, results) => {
                if (err) {
                    throw err;
                } else {
                    res.render('concluidos/index', {
                        resultados: results[0],
                        estados: results[1],
                        usuario: req.user,
                        activoCon: 1
                    });
                }
            });
        }
    };

    router.get('/index', isLoggedIn, (routes.index, routes.notificaciones));
    return router;
};

//MUESTRA EL HISTORIAL
module.exports.post_concl_historialComent = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {
        "coment": "",
        "msj": ""
    };

    connection.query("SELECT comentario.fecha, comunicacion.nombre as comunicacion, actividad.nombre as nombreAct, comentario.texto, usuario.nombre, usuario.apellido FROM comentario, usuario, comunicacion, actividad WHERE comentario.idUsuario=usuario.id AND comunicacion.id=comentario.idComunicacion AND comentario.idActividad=actividad.id AND comentario.idNotificacion=? ORDER BY fecha DESC;", [idNot2], function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["coment"] = rows;
                res.json(data);
            } else {
                data["msj"] = 'No existen comentarios en la notificación';
                res.json(data);
            }
        }
    });
}

//SUBE LOS COMENTARIOS
module.exports.post_agregarComent = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot = req.body.idNot;
    let estado = req.body.estado;
    let user = req.user.id;

    if (user === "" || user === false) {
        req.flash('error', 'Se encontró un error en el proceso');
        res.redirect(index + 'concluidos/index');
    } else {
        connection.query("UPDATE notificacion SET estado =? WHERE id =?", [estado, idNot], function(err, result) {
            if (err) {
                req.flash('error', 'Se encontró un error en el proceso');
                res.redirect(index + 'concluidos/index');
            } else {
                req.flash('success', 'El comentario se agregó correctamente');
                res.redirect(index + 'concluidos/index');
            }
        });
    }
}

//Borra notificaciones (los manda a papelera)
module.exports.borrar_notificacion = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot = req.body.idNot3;

    connection.query("UPDATE notificacion SET estado=4 WHERE id=?", [idNot], function(err, result) {
        if (err) {
            req.flash('error', 'Se encontró un error en el proceso');
            res.redirect(index + 'concluidos/index');
        } else {
            req.flash('success', 'La notificación se eliminó correctamente');
            res.redirect(index + 'concluidos/index');
        }
    });


}

//Traer notificaciones 
module.exports.traer_notificaciones_concl = function(req, res) {
    res.setHeader('Content-type', 'text/plain');

    var data = {
        "results": ""
    };

    let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=3 ORDER BY estado ASC, id DESC;";

    connection.query(sql, function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["results"] = rows;
                res.json(data);
            }
        }
    });
}

module.exports.enviar_papelera = function(req, res) {
    res.setHeader('Content-type', 'text/plain');

    connection.query("UPDATE notificacion SET estado=4 where estado=3;", function(err, result) {
        if (err) {
            console.log(err);
            req.flash('error', 'Se encontró un error en el proceso');
            res.redirect(index + 'concluidos/index');
        } else {
            req.flash('success', 'Las notificaciones fueron enviados a papelera correctamente');
            res.redirect(index + 'concluidos/index');
        }
    });
}