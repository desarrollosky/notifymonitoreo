const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');

const index = "/notifyMonitoreo/";

module.exports = function(express, globals) {
    const router = express.Router();
    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Ayuda',
                globals: encodeURIComponent(JSON.stringify(globals)),
                usuario: req.user,
                activoMan: 1
            };
            res.render('ayuda/index', content);
        }
    };

    router.get('/index', isLoggedIn, routes.index);
    return router;
};