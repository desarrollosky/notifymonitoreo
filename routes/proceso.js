const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');

const index = "/notifyMonitoreo/";

module.exports = function(express, globals) {
    const router = express.Router();
    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Proceso',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('proceso/index', content);
        },
        notificaciones: function(req, res, next) {
            let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=2 ORDER BY estado ASC, id DESC;";
            sql += "SELECT * FROM estado LIMIT 3;";
            sql += "SELECT * FROM actividad where nombre != 'Otra' order by nombre asc;";
            sql += "SELECT * FROM comunicacion;";

            let query = connection.query(sql, (err, results) => {
                if (err) {
                    throw err;
                } else {
                    res.render('proceso/index', {
                        resultados: results[0],
                        estados: results[1],
                        actividades: results[2],
                        comunicacion: results[3],
                        usuario: req.user,
                        activoPro: 1
                    });
                }
            });
        }
    };

    router.get('/index', isLoggedIn, (routes.index, routes.notificaciones));
    return router;
};

//Traer notificaciones 
module.exports.traer_notificaciones_proc = function(req, res) {
    res.setHeader('Content-type', 'text/plain');

    var data = {
        "results": ""
    };

    let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=2 ORDER BY estado ASC, id DESC;";

    connection.query(sql, function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["results"] = rows;
                res.json(data);
            }
        }
    });
}

//MUESTRA EL HISTORIAL
module.exports.post_process_historialComent = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {
        "coment": "",
        "msj": ""
    };

    connection.query("SELECT comentario.fecha, comunicacion.nombre as comunicacion, actividad.nombre as nombreAct, comentario.texto, usuario.nombre, usuario.apellido FROM comentario, usuario, comunicacion, actividad WHERE comentario.idUsuario=usuario.id AND comunicacion.id=comentario.idComunicacion AND comentario.idActividad=actividad.id AND comentario.idNotificacion=? ORDER BY fecha DESC;", [idNot2], function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["coment"] = rows;
                res.json(data);
            } else {
                data["msj"] = 'No existen comentarios en la notificación';
                res.json(data);
            }
        }
    });
}

//CAMBIA EL ESTADO
module.exports.post_cambiarEst = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot = req.body.idNot;
    let actividad = req.body.actividad;
    let estado = req.body.estado;
    let aviso = req.body.aviso;
    let comunicacion = req.body.comunicacion;
    let comentarios = req.body.comentarios;
    let user = req.user.id;

    var datetime = new Date();
    var day = datetime.getDate();
    var month = datetime.getMonth() + 1;
    var year = datetime.getFullYear();
    var hour = datetime.getHours();
    var minutes = datetime.getMinutes();
    var seconds = datetime.getSeconds();

    var fecha = year + '-' + month + '-' + day + ' ' + hour + ':' + minutes + ':' + seconds;

    if (user === "" || user === false) {
        req.flash('error', 'Se encontró un error en el proceso');
        res.redirect(index + 'proceso/index');
    } else {
        if (aviso === "si") {
            connection.query("INSERT INTO comentario (idNotificacion, idUsuario, idComunicacion, idActividad, texto, fecha) VALUES (?, ?, ?, ?, ?, ?); UPDATE notificacion SET estado =?, texto=? WHERE id =?", [idNot, user, comunicacion, actividad, comentarios, fecha, estado, comentarios, idNot], function(err, result) {
                if (err) {
                    req.flash('error', 'Se encontró un error en el proceso');
                    res.redirect(index + 'proceso/index');
                } else {
                    req.flash('success', 'El comentario se agregó correctamente');
                    res.redirect(index + 'proceso/index');
                }
            });
        } else {
            connection.query("INSERT INTO comentario (idNotificacion, idUsuario, idComunicacion, idActividad, texto, fecha) VALUES (?, ?, ?, ?, ?, ?); UPDATE notificacion SET estado =?, texto=? WHERE id =?", [idNot, user, '5', actividad, comentarios, fecha, estado, comentarios, idNot], function(err, result) {
                if (err) {
                    req.flash('error', 'Se encontró un error en el proceso');
                    res.redirect(index + 'proceso/index');
                } else {
                    req.flash('success', 'El comentario se agregó correctamente');
                    res.redirect(index + 'proceso/index');
                }
            });
        }
    }
}

//Borra notificaciones (los manda a papelera)
module.exports.borrar_notificacion = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot = req.body.idNot3;

    connection.query("UPDATE notificacion SET estado=4 WHERE id=?", [idNot], function(err, result) {
        if (err) {
            req.flash('error', 'Se encontró un error en el proceso');
            res.redirect(index + 'proceso/index');
        } else {
            req.flash('success', 'La notificación se eliminó correctamente');
            res.redirect(index + 'proceso/index');
        }
    });
}