const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');
//const xlsx = require("xlsx");
//var FileSaver = require('file-saver');

module.exports = function(express, globals) {
    const router = express.Router();
    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Descaargar',
                globals: encodeURIComponent(JSON.stringify(globals)),
                usuario: req.user,
                activoDes: 1
            };
            res.render('descargar/index', content);
        },
        info: function(req, res, next) {
            let sql = "SELECT id, nombre FROM estado WHERE id=1 OR id=2 OR id=3; SELECT id, nombre, apellido FROM usuario WHERE idRol='3'; SELECT DISTINCT idUnidad FROM notificacion;";

            let query = connection.query(sql, (err, results) => {
                if (err) {
                    throw err;
                } else {
                    res.render('descargar/index', {
                        estados: results[0],
                        monitoristas: results[1],
                        unidades: results[2],
                        usuario: req.user,
                        activoDes: 1
                    });
                }
            });
        }
    };

    router.get('/index', isLoggedIn, (routes.index, routes.info));
    return router;
};

module.exports.downloadExcel = function(req, res) {
    res.setHeader('Content-type', 'text/plain');

    let tipo = req.body.tipo;
    let estado = req.body.estado;
    let monitor = req.body.monitor;
    let unidad = req.body.unidad;
    let inicio = req.body.inicio;
    let final = req.body.final;

    var stringEst = '';
    var stringUnid = '';
    var stringMonit = '';
    var stringClass = '';

    inicio = "'" + inicio + " 00:00:00'";
    final = "'" + final + " 23:59:59'";

    var data = {
        "alerta": "",
        "clase": "",
        "rows": ""
    };

    if (estado === '0') {
        stringEst = 'estado!=4';
    } else {
        stringEst = 'estado=' + estado;
    }

    if (unidad === '0') {
        stringUnid = '';
    } else {
        stringUnid = "AND idUnidad='" + unidad + "'";
    }

    if (monitor === '0') {
        stringMonit = '';
    } else {
        stringMonit = "AND (SELECT comentario.idUsuario FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id AND comentario.idUsuario='" + monitor + "' ORDER BY fecha DESC LIMIT 1)";
    }

    if (tipo === 'excel') {
        stringClass = '';
    } else if (tipo === 'muestra') {
        stringClass = '(SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS claseEst,';
    }

    if (final !== "" && inicio !== "") {
        var sql = "SELECT fecha as Fecha,(SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS Estado, " + stringClass + " idUnidad as IdUnidad, nombre as NombreNotificacion," 
        + "latitude as Latitud, longitude as Longitud, grupo as Grupo, (SELECT nombre FROM comentario, actividad WHERE comentario.idNotificacion=notificacion.id "
        + "AND comentario.idActividad=actividad.id ORDER BY fecha DESC LIMIT 1) AS 'Actividad de la Unidad', (SELECT nombre FROM comentario, comunicacion WHERE comentario.idNotificacion=notificacion.id AND "
        + "comentario.idComunicacion=comunicacion.id ORDER BY fecha DESC LIMIT 1) AS '¿Operador Aviso?', texto as 'Accion realizada por Monitoreo CCT',(SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE "
        + "comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as 'Tiempo de atencion', (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE "
        + "comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY "
        + "fecha ASC LIMIT 1))) AS AUC, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE "
        + "comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS Usuario FROM notificacion WHERE " + stringEst + " " 
        + stringMonit + " " + stringUnid + " AND fecha BETWEEN " + inicio + " AND " + final + "  and (substring(notificacion.nombre,1,3)) != '[M]' ORDER BY estado ASC, id DESC;"


       /* var sql = "SELECT fecha as Fecha,(SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS Estado, " + stringClass + " idUnidad as IdUnidad, nombre as NombreNotificacion," 
        + "latitude as Latitud, longitude as Longitud, grupo as Grupo,  (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE "
        + "comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as APC, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE "
        + "comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY "
        + "fecha ASC LIMIT 1))) AS AUC, (SELECT nombre FROM comentario, comunicacion WHERE comentario.idNotificacion=notificacion.id AND "
        + "comentario.idComunicacion=comunicacion.id ORDER BY fecha DESC LIMIT 1) AS Aviso, (SELECT nombre FROM comentario, actividad WHERE comentario.idNotificacion=notificacion.id "
        + "AND comentario.idActividad=actividad.id ORDER BY fecha DESC LIMIT 1) AS Actividad, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE "
        + "comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS Usuario, texto as 'Accion realizada por Monitoreo CCT' FROM notificacion WHERE " + stringEst + " " 
        + stringMonit + " " + stringUnid + " AND fecha BETWEEN " + inicio + " AND " + final + "  and (substring(notificacion.nombre,1,3)) != '[M]' ORDER BY estado ASC, id DESC;"*/

        connection.query(sql, function(err, rows, fields) {
            if (err) { throw err; } else {
                if (rows.length != 0) {
                    data["rows"] = rows;
                    res.json(data);
                } else {
                    data["alerta"] = 'No se encontraron registros.';
                    data["clase"] = 'alert-warning';
                    res.json(data);
                }
            }
        });
    } else {
        data["alerta"] = 'Los campos no deben estar vacíos.';
        data["clase"] = 'alert-danger';
        res.json(data);
    }
}