const connection = require('../config/db');
const { isLoggedIn } = require('../config/auth');

const index = "/notifyMonitoreo/";

module.exports = function(express, globals) {
    const router = express.Router();
    routes = {
        index: function(req, res, next) {
            content = {
                title: 'Papelera',
                globals: encodeURIComponent(JSON.stringify(globals)),
            };
            res.render('papelera/index', content);
        },
        notificaciones: function(req, res, next) {
            let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=4 ORDER BY estado ASC, id DESC;";
            sql += "SELECT * FROM actividad where nombre != 'Otra' order by nombre asc;";
            sql += "SELECT * FROM estado;";
            sql += "SELECT * FROM comunicacion;";

            let query = connection.query(sql, (err, results) => {
                if (err) {
                    throw err;
                } else {
                    res.render('papelera/index', {
                        resultados: results[0],
                        actividades: results[1],
                        estados: results[2],
                        comunicacion: results[3],
                        usuario: req.user,
                        activoPap: 1
                    });
                }
            });
        }
    };
    router.get('/index', isLoggedIn, (routes.index, routes.notificaciones));

    return router;
};

module.exports.borrar_notif = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot = req.body.notificacionID;

    connection.query("DELETE FROM comentario WHERE idNotificacion=?; DELETE FROM notificacion WHERE id=?;", [idNot, idNot], function(err, result) {
        if (err) {
            console.log(err);
            req.flash('error', 'Se encontró un error en el proceso');
            res.redirect(index + 'papelera/index');
        } else {
            req.flash('success', 'La notificación se eliminó correctamente');
            res.redirect(index + 'papelera/index');
        }
    });
}

module.exports.delete_All = function(req, res) {
    res.setHeader('Content-type', 'text/plain');

    connection.query("DELETE comentario FROM comentario, notificacion WHERE notificacion.estado=4 AND comentario.idNotificacion=notificacion.id; DELETE FROM notificacion WHERE estado=4;", function(err, result) {
        if (err) {
            console.log(err);
            req.flash('error', 'Se encontró un error en el proceso');
            res.redirect(index + 'papelera/index');
        } else {
            req.flash('success', 'La papelera se vació correctamente');
            res.redirect(index + 'papelera/index');
        }
    });
}

module.exports.traer_notificaciones_pap = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idRol = req.user.idRol;

    var data = {
        "results": "",
        "userRol": ""
    };

    let sql = "SELECT id, idUnidad, nombre as nombreNotif, texto, latitude, longitude, grupo, fecha, (SELECT nombre FROM estado WHERE estado.id=notificacion.estado) AS nombreEst, (SELECT clase FROM estado WHERE estado.id=notificacion.estado) AS clase, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1), fecha)) as apc, (SELECT TIMEDIFF((SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha DESC LIMIT 1), (SELECT fecha FROM comentario WHERE comentario.idNotificacion=notificacion.id ORDER BY fecha ASC LIMIT 1))) AS auc, (SELECT CONCAT(nombre,' ',apellido) FROM comentario, usuario WHERE comentario.idNotificacion=notificacion.id AND comentario.idUsuario=usuario.id ORDER BY fecha DESC LIMIT 1) AS usuario FROM notificacion WHERE estado=4 ORDER BY estado ASC, id DESC;";

    connection.query(sql, function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["results"] = rows;
                data["userRol"] = idRol;
                res.json(data);
            }
        }
    });
}

module.exports.mostrarHistorial = function(req, res) {
    res.setHeader('Content-type', 'text/plain');
    let idNot2 = req.body.id;
    var data = {
        "coment": "",
        "msj": ""
    };

    connection.query("SELECT comentario.fecha, comunicacion.nombre as comunicacion, actividad.nombre as nombreAct, comentario.texto, usuario.nombre, usuario.apellido FROM comentario, usuario, comunicacion, actividad WHERE comentario.idUsuario=usuario.id AND comunicacion.id=comentario.idComunicacion AND comentario.idActividad=actividad.id AND comentario.idNotificacion=? ORDER BY fecha DESC;", [idNot2], function(err, rows, fields) {
        if (err) { throw err; } else {
            if (rows.length != 0) {
                data["coment"] = rows;
                res.json(data);
            } else {
                data["msj"] = 'No existen comentarios en la notificación';
                res.json(data);
            }
        }
    });
}