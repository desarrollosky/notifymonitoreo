'use strict';

const mysql = require('mysql');

var connection = mysql.createConnection({
    host     : 'localhost',
    port     : '3307',
    user     : 'root', 
    password : 'S3rv3r2021!',
    database : 'nuevogestor_monitoreo',
    dateStrings: 'date',
    multipleStatements: true
});

connection.connect(function(err) {
    if (err) {
        console.error(err.message);
    } else {
        console.log('Connection success');
    }
});

module.exports = connection;